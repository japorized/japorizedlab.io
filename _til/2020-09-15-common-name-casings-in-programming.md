---
layout: post
type: til
title: Common Name Casings in Programming
tags: ['coding']
comments-enabled: true
---

I can never remember the names of the commonly used name casings, so I'll be
noting them down here for myself.

Note that this isn't an extensive list. The Comments are simply to help myself
remember them in some way, for a slightly longer period.

<!-- more -->

| Name Casing | Example    | Comments                                                                                                                                                                                                       |
| :---------- | :--------- | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Kebab case  | kebab-case | The hyphens look like a stick skewering the words (meat), so the whole thing look like a kebab.                                                                                                                |
| Snake case  | snake_case | The underscores look like the body of a snake lazily dragging on the ground.                                                                                                                                   |
| Camel case  | camelCase  | Each of the capitalized letter looks like the hump of a camel.                                                                                                                                                 |
| Pascal case | PascalCase | Subset of Camel case, where first letter is capitalized. Popularized by the [Pascal Programming Language](https://docs.microsoft.com/en-us/archive/blogs/brada/history-around-pascal-casing-and-camel-casing). |
{:class="fullwidth has-border"}
