---
layout: post
type: til
title: Date parsing by JavaScriptCore
tags: ['coding' ]
comments-enabled: true
---

Not today, but last night, I was made aware of the fact that JavaScriptCore,
which is the JavaScript engine made and used by Apple in their browsers does
Date parsing from string a little differently from SpiderMonkey (Mozilla) and
V8 (Google). In particular, on SpiderMonkey and V8,

```javascript
new Date("1950-1-1") // returns Date Sun Jan 01 1950 07:30:00 GMT+0...
```

which is a proper `Date` object with timezone. On the other hand, on
JavaScriptCore

```javascript
new Date("1950-1-1") // returns Invalid Date
```
