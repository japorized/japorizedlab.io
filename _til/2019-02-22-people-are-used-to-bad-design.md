---
layout: post
type: til
title: People are used to bad design
tags: [ 'design' ]
comments-enabled: true
---

> The public is more familiar with bad design than good design. It is, in effect,
conditioned to prefer bad design, because that is what it lives with. The new becomes
threatening, the old reassuring.  
~ Paul Rand

In some sense, people are conditioned into believing bad design is a part of life, a
reality that we cannot escape. Sadly, for most, the belief stops there. Not many go
beyond that belief, and come out saying "We can do better!"

There is no excuse for bad design; it is usually a baseless affinity to the familiar, and
a lack of desire to get your hands dirty.

> A designer knows he has achieved perfection not when there is nothing left to add, but
when there is nothing left to take away.  
~ Antoine de Saint-Exupéry

Cliché quotes? I thought so some time ago, but I now have newfound respect and
understanding for them, as they strongly resonate with my own experiences and belief.
