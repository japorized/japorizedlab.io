---
layout: post
title: Primitive Element Theorem
description: We look into the proof of the Primitive Element Theorem, reverse-engineer a logical reason of how to push forward with the proof.
toc: true
comments-enabled: true
anchors: [ "The Theorem", "Planning for the proof", "The Clean Proof", "Post-mortem" ]
---

## Preliminary

First, let's have some preliminaries to the Primitive Element Theorem.

### Separable Extension

We say that the extension $$K$$ of a field $$F$$ {% sidenote
'sn-notation-for-field-ext' 'We usually write $K/F$ for $K$ being an extension
of $F$.' %} is separable if it is  
* algebraic: i.e. each of its elements is a root of some polynomial of $$F[x]$$;
and
* each $$\alpha \in K$$ is separable, i.e. if $$p(x)$$ is the minimal polynomial
of $$\alpha$$ over $$F$$, then $$p(x)$$ completely factors in $$K[x]$$.

### Simple Extension

We say that the extension $$K$$ of a field $$F$$ is simple if it is expressible
as $$K = F(\alpha)$$ for some $$\alpha \in K$$, where $$F(\alpha)$$ means
adjoining $$\alpha$$ to the field $$F$$, i.e

$$ F(\alpha) = \text{span}_{F} \{ 1, \alpha, \ldots, \alpha^{n - 1} \}, $$

where $n$ is the degree of the minimal polynomial of $\alpha$.

### Finite Extension

We say that the extension $$K$$ over $$F$$ is finite if

$$K = F(\alpha_1, \alpha_2, \ldots, \alpha_n) = F(\alpha_1)(\alpha_2)...(\alpha_n).$$

---

## The Theorem

The Primitive Element Theorem is stated as follows:

> If $$K / F$$ is finite and separable, then $$K / F$$ is simple.

A short statement, but important nonetheless. For example, it implies that
finite extensions of *perfect fields* {% sidenote 'sn-defn-perfect-field' 'We
say that a field $$F$$ is perfect if every one of its algebraic extension is
separable.' %} are simple, which helps us realize that,
* fields of *characteristic 0* {% sidenote 'sn-char-0-fields' 'A field has
characteristic 0 is a field where no matter how many times we add 1 to 1, we
will never get 0.' %} always have simple extensions, and
* any extension of a finite field is always just one algebraic number away.

{% marginnote 'mn-warning-for-clean-proof' '
If you skip ahead and try to read the [clean proof](#the-clean-proof), you will
probably be stumped by the weirdly defined $$S$$ that came out of almost
nowhere. You will also be stumped by how a $$k(x)$$ is suddenly defined in terms
of $$p(x)$$. And why oh why did we think about choosing this extension $$L$$ in
the first place? At first glance, these seem like a struck of genius, but these are
choices selected well within careful reasoning that requires some actual hands-on
into what we are given with.'
%}

---

## The Proof

### Planning for the proof

If we want $$F(\alpha, \beta) = F(\gamma)$$ for some $$\gamma$$ in some
extension, one naive choice is to go with $$\gamma = \alpha + u \beta$$ for some
$$u \in F^\times$$ {% sidenote 'sn-units-of-F' '$$F^\times$$ denotes the set of
units of $$F$$' %} and hope that this will force $$\alpha, \beta \in F(\gamma)$$.
The argument is similar for either $\alpha$ or $\beta$ (by switching variables),
so let's think about only one of them. Let $$p(x)$$ and $$q(x)$$ be the minimal
polynomials of $$\alpha$$ and $$\beta$$, respectively. Now $$q(x)$$ is not
necessarily a minimal polynomial of $\beta$ over $F(\gamma)$, so let's make use
of that.

If $$\beta \in F(\gamma)$$, then we must have $$x - \beta \mid q(x)$$. So let's
consider the minimal polynomial $$h(x)$$ of $$\beta$$ in $$F(\gamma)$$, which
would divide $$q(x)$$. Of course, ideally, we want $$h(x) = x - \beta$$.  Then
let's suppose that $$h(x)$$ has some root other than $$\beta$$.

Then in the splitting field of $$q(x)$$, where $$h(x)$$ must then also split,
since $$q(x)$$ is separable, we have that $$h(x)$$ must therefore be able
to split into linear terms, where each linear term has a root of $$q(x)$$ as its
constant value. In other words, all roots of $$h(x)$$ are roots of $$q(x)$$.

Then we notice another possible polynomial that such an $$h(x)$$ can divide: we
know that $$\alpha = \gamma - u \beta$$, and $$\alpha$$ is a root of $$p(x)$$.
Then if we let $$k(x) = p(\gamma - u x)$$, we have

$$k(\beta) = p(\gamma - u \beta) = p(\alpha) = 0.$$

So $$h(x) \mid k(x)$$. Now since all the roots of $$h(x)$$ are roots of $$q(x)$$,
these roots must also be roots of $$k(x)$$. Let these other roots of $$q(x)$$ be
labelled $$\beta_j$$'s. Then picking $$\beta_j \neq \beta$$, we have

$$k(\beta_j) = 0 \iff p(\gamma - u \beta_j) = 0.$$

We already know what the roots of $$p(x)$$ are so let's label those as
$$\alpha_i$$. Then

$$\gamma - u \beta_j = \alpha_i.$$

Note that $$\alpha_i \neq \alpha$$ since the roots are unique. Following that,

$$\alpha + u \beta - u \beta_j = \alpha_i,$$

which then

$$u = \frac{\alpha_i - \alpha}{\beta - \beta_j}. \qquad (*)$$

We notice that there are only finitely many such $$u$$'s in $F^\times$ since
there are only as many as the roots $$\alpha_i$$'s and $$\beta_j$$'s can allow.
However, $$F^\times$$ is infinite by our assumption, i.e. there are always units
of $$F$$ that cannot be expressed as in $$(*)$$.

So by picking a $$u \in F^\times$$ that is not determined by
$$(*)$$, we rule out the possibility that $$k(x)$$ has these other $$\beta_j$$'s
as roots, and hence forcing $h(x)$ to be what we want: that is $$h(x) = x -
\beta$$. Thus our job is done for showing that $$\beta \in F(\gamma)$$!

We can then apply the same argument to showing that $$\alpha \in F(\gamma)$$, by
letting $$\gamma = \beta + u' \alpha$$ by choosing $u'$ in a similar fashion as
above. In this case, we would have to extend our working field to the
splitting field of $$p(x)$$.

Then to put the two together, we could have then started working with an
extension where both $$p(x)$$ and $$q(x)$$ splits, and the splitting field of
$$p(x) q(x)$$ is exactly where we should be working in.

### The Clean Proof

If $$F$$ is finite, then $$K$$ itself is finite, and in particular it is
generated by some $$\alpha \in K$$, i.e. $$K = \langle \alpha \rangle$$. So
$$K = F(\alpha)$$. This is the easy case.

Suppose $$F$$ is infinite, which then implies that $$K$$ is infinite. Since the
extension is finite, we may assume that

$$K = F(\eta_1, \eta_2, \ldots, \eta_n),$$

where the $$\eta_i$$'s are algebraic over $$F$$. Note that it is sufficient for
us to show that $$K = F(\eta_1, \eta_2) = F(\gamma)$$ for some $$\gamma \in K$$,
since we may then repeatedly apply the same argument for each of the adjoined
elements. For simplicity, let's write $$\eta_1 = \alpha$$ and $$\eta_2 =
\beta$$.

Now let $$L$$ be the splitting field of $$p(x) q(x)$$ over $$K$$. Let the roots
of $$p(x)$$ be

$$\alpha = \alpha_1, \, \alpha_2, \, \ldots, \, \alpha_n,$$

and the roots of $$q(x)$$ be

$$\beta = \beta_1, \, \beta_2, \, \ldots, \, \beta_m.$$

By separability, $$\alpha_i \neq \alpha_j$$ and $$\beta_i \neq \beta_j$$ for all
$$i \neq j$$. Now let {% sidenote 'sn-alternative-choice-of-S' 'Note that had we
wanted to start with $$\gamma = \beta + u \alpha$$, we would have declared $S$
with elements like $$\frac{\beta_j - \beta_1}{\alpha_1 - \alpha_i}$$.' %}

$$S := \left\{ \frac{\alpha_i - \alpha_1}{\beta_1 - \beta_j} : 1 < i \leq n, \, 1 < j \leq m \right\}.$$

Since $$S$$ is finite while $$F$$ is infinite, $$\exists u \in F^\times$$ such
that $$u \notin S$$. Let $$\gamma = \alpha + u \beta$$.

We now claim that $$F(\alpha, \beta) = F(\gamma)$$. Clearly, $$F(\gamma) \subseteq
F(\alpha, \beta)$$ since $$\gamma \in F(\alpha, \beta)$$. Let $$h(x)$$ be the
minimal polynomial of $$\beta$$ over $$F(\gamma)$$. Since $$q(\beta) = 0$$, we have
that $$h(x) \mid q(x)$$, and consequently if $$h(\eta) = 0$$, then $$\eta =
\beta_j$$ for some $$j \in \{ 1, \ldots, m \}$$.

Now let $$k(x) = p(\gamma - ux) \in F(\gamma)[x]$$. Notice that

$$k(\beta) = p(\gamma - u \beta) = p(\alpha) = 0.$$

Thus $$h(x) \mid k(x)$$. Notice that for $$j > 1$$, we have

$$\begin{aligned}
  k(\beta_j) = 0 &\iff p(\gamma - u \beta_j) = 0 \\
                 &\iff \gamma - u \beta_j = \alpha_i \text{ for some } i \\
                 &\iff \alpha_1 + u \beta_1 - u \beta_j = \alpha_i \\
                 &\iff u = \frac{\alpha_i - \alpha_1}{\beta_1 - \beta_j} \in
                 S.
\end{aligned}$$

Thus we know that for these $$j$$'s, $$k(\beta_j) \neq 0$$ since we chose $$u
\notin S$$.

It follows that $$h(\beta_j) \neq 0$$ for $$j > 1$$, and so $$h(x) = x - \beta \in
F(\gamma)[x]$$, implying that $$\beta \in F(\gamma)$$. Using the same argument,
we can show that $$\alpha \in F(\gamma)$$. This completes the proof,

---

## Post-mortem

### Math

This is indeed a very profound result, making use of relatively simple notions
such as minimal polynomials and splitting fields, and then proving for us a
theorem that helps us narrow down the choice of the algebraic number to a single
number that extends the base field to the extension.

### Non-math

I am usually not someone who is satisfied with just being told that a given
solution works because it does. There are many times where the solution is right
before us, just buried in some soil or covered with some thick layer of dust
that it is not immediately clear why or how someone knew what was hidden behind
it. Calling someone smart or a genius in these cases is somewhat off-putting, in
that it glosses over their hard work and, perhaps, just having slightly more
patience than we do.
