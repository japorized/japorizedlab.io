---
layout: post
type: math
title: Weierstrass Continuity
description: We look into a weird consequence of the definition of continuity given by Karl Weierstrass.
toc: true
comments-enabled: true
anchors: [ "Definition of Weierstrass Continuity", "Problem Analysis", "Conclusion" ]
---

We know that there are certain definitions and results in mathematics that lead
to unintuitive consequences, some mild, which just requires a bit of tweak in
perspective, and others straight up mind-boggling. I came across one of the
former today, {% sidenote 'sn-problem' 'See
[MathSE](https://math.stackexchange.com/questions/68800/functions-which-are-continuous-but-not-bicontinuous).
' %} which initially shoke me so much I felt like a truck hit me and
had almost sent me down a very badly-timed existential crisis {% sidenote
'sn-badly-timed' 'This is a few days right before an important exam.' %}.

I will not review what Weierstrass continuity means first, but hint that it is
the definition of continuity that uses $\epsilon$'s and $\delta$'s. If you are
not familiar with it, this post will be uninteresting if you follow the
sequence of the writing, so look downwards for the definition of Weierstrass
continuity. If you are aware of the definition, follow along.

{% marginsvg 'mn-harmless' "math/2019-04-08-weierstrass-continuity/harmless-looking-graph.svg" "Graph of f(x)" %}
Consider the function $f : [0, 1) \cup [2, 3] \to [0, 2]$ given by

$$
  f(x) = \begin{cases}
    x     & x \in [0, 1) \\
    x - 1 & x \in [2, 3]
  \end{cases}
$$

The graph of $f(x)$ is shown to the right. This map looks discontinuous alright,
but is it really discontinuous?

Consider $c = 2$. Let $\epsilon > 0$. Let us focus on left continuity at $c$,
since that is the point that disturbs us. Let us choose $\delta = 1 + \epsilon >
0$. Now for any $y \in [0, 1) \cup [2, 3]$, since we are focusing on left
continuity, we know that $f(y) = y$. Now if $\abs{c - y} = 2 - y < \delta$, we
have that $1 - y < \epsilon$. Then, observe that

$$\abs{f(2) - f(y)} = 1 - y < \epsilon.$$

By definition, $f$ is indeed continuous at $c = 2$.

The keen and trained eye would immediately recognize where things have went
wrong, but the innocent will most likely exclaim, "Hold up! WTF!?"

---

I shall now present the definition the Weierstrass' definition of continuity.

## Definition of Weierstrass Continuity

Weierstrass' flavour of continuity is one that those who have taken a serious
course in Calculus should be familiar with.

Let $f : X \to Y$ be a function between two (metric) spaces (or just sets) $X$
and $Y$, and let $x \in X$. We (Weierstrass) say(s) that $f$ is continuous on
$x$ if

$$\forall \epsilon > 0 \enspace \exists \delta > 0 \enspace \forall y \in X$$

$$|x - y| < \delta \implies |f(x) - f(y)| < \epsilon$$

---

## Problem Analysis

The problem lies in that misleading graph: the domain of $f(x)$ is a disjoint
union, not a single "connected set" {% sidenote 'sn-connectedness'
'Connectedness is important in analysis. It is actually non-trivial to define
what it means for a set to be connected. Here is a relevant
[article](https://blogs.scientificamerican.com/roots-of-unity/a-few-of-my-favorite-spaces-the-topologist-s-sine-curve/)
about connectedness.' %}. Therefore, the usual Cartesian plane is not a good
representation of the graph. In particular, we would have to truncate the area
between 1 and 2, and one would then immediately notice (or at least agree) that
$f$ is indeed continuous.

Of course, the choice of $\delta$ is particularly awkward: it is not applicable
to the right side of $c = 2$, since $y = 3$, in particular, would not work for
$\epsilon = 0.2 > 0$. This is a consequence of assuming the usual sense of
distance between points, or
[metric](https://en.wikipedia.org/wiki/Metric_(mathematics)), on the real
numbers $\mathbb{R}$ (and its subsets). To make things work nicely again, i.e.
to be able to choose a nicer $\delta$ that works on both sides, we would either
have to define a new metric, or we can "identify" everything between 1 and 2 as
simply 2. In other words, we are looking at a world where $0 + 1 = 2$.

Note that there is no problem in embedding the graph $f$ onto the usual
Cartesian plane. It still helps us visualize how the function looks like on the
usual world in our normal sense of distances.

---

## Conclusion

Weierstrass' definition of continuity transcends our usual sense of distance and
even rectified itself, to some extent, in this particular example. I now have
newfound appreciation and respect for the thought that has gone into this way of
thinking about continuity.
