---
layout: post
type: math
title: A brief revisit of Gödel's Incompleteness Theorems
description:
  I had a sudden recall about Gödel's Incompleteness Theorems,
  decided to give them some thought, and see what I can gain from this mental venture.
  Some of these thoughts may be controversial. You have been warned.
comments-enabled: true
---

Gödel's Incompleteness Theorems, particularly the second of the two,
stated that any system that contains the arithmetic, i.e. discusses about
statements regarding the natural numbers, cannot demonstrate its own
[consistency](https://en.wikipedia.org/wiki/Consistency).
The first of the two states that there will always be statements about
the natural numbers that will not be provable by whatever axiomatic system
that we construct to be consistent.
One may continue adding more and more statements about the natural numbers,
but it will never reach a state of
[completeness](https://en.wikipedia.org/wiki/Complete_theory).

This sounds rather discouraging, especially when there are so many unsolved
number theoretic problems that are out in the wild. And many more can easily
sprout. Does there exist some identifiable pattern amongst numbers divisible by 7,
just as those divisible by 3? If there are infinitely many twin primes,
{% sidenote 'sn-twin-prime' 
'Vis-à-vis the [Twin Prime Conjecture](https://en.wikipedia.org/wiki/Twin_prime).'%} 
how does the distribution look like? Could there exist "triplet" primes other than
3, 5, and 7?
{% sidenote 'sn-triplet-primes-confusion'
'Not to be confused with [Prime Triplets](https://en.wikipedia.org/wiki/Prime_triplet).' %}
Are there also infinitely many of these?

Or was it discouraging? I mean, this does sort of mean that we have an
unimaginably large playing field, that we might never exhaust in almost any
of our lifetimes. For those with the heart of a child, you basically have
a toy whose functionalities you will never finish exploring.

I have mixed feelings about this; on one hand, we seem to have been promised a
seemingly-infinite sandbox of which we can never finish constructing,
while on the other, for those with an inquisitive mind, the fact that we never
perceive its boundaries is rather unsatisfactory.

---

One may then ask if we should forgo the path of seeking a satisfactory axiomatic system,
despite the Incompleteness Theorems. From the outside,
{% sidenote 'sn-i-am-an-outsider' 'I consider myself an outsider, since I do not
partake in its research. I am merely an insignificant interloper.' %}
it seems that the formation of axiomatic systems is still widely adopted(?),
with Set Theory being a very central cornerstone of modern theoretical mathematics.
I do not think that I have a good answer, but these axiomatic systems, while incomplete,
have been extremely helpful in achieving a rather deep understanding of various topics
in mathematics.

However, it may be that one day, somebody comes up and find out some statement
that ends in a contradiction in our favorite systems, rendering them inconsistent.
That is a somewhat scary thought, and I can see some of my friends who will be deeply bothered
by this, and potentially trigger a very serious case of existential crisis in some of us.
{% sidenote 'sn-post-note-on-existential-crisis' 'I must emphasize that this is solely based
on my limited and flawed knowledge about formal systems.' %}

---

On another note, one result that baffled my mind for quite a while was the following,
until reading it again today. The following quote is from Wikipedia.

> The theory of algebraically closed fields of a given characteristic is complete,
> consistent, and has an infinite but recursively enumerable set of axioms.
> **However it is not possible to encode the integers into this theory,
> and the theory cannot describe arithmetic of integers.**

Perhaps I was too dumb to understand why when I came across this result first in the lectures
that I've attended. Without the bolded statement, I thought that something was seriously
amiss as the first sentence was seemingly a counter-example to the Incompleteness Theorems.
Turns out it was an unwarranted worry, and I was dumb. I did not fully understand the
Incompleteness Theorems from their syntactic statements, and end up hurting myself in my confusion.

In case it was not clear, the bolded statement also means that the theory of algebraically closed
fields was not a theory that encloses all of arithmetic. This is why the Incompleteness Theorems
did not apply to it, and we have ourselves a
"complete, consistent, and has an infinite but recursively enumerable set of axioms"
*seemingly* "about natural numbers".
