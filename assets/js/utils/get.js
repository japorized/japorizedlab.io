/**
 * GET request as a Promise
 *
 * @param     {string}      path          URL or path to JSON file
 * @param     {string}      type = ''     Response type
 *
 * @return    {Promise}
 */
export function get(path, type = '') {
  return new Promise(function (resolve, reject) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', path, true);
    xhr.responseType = type;
    xhr.onload = function () {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        if (xhr.status === 200) {
          resolve({
            response: xhr.response,
            headers: {
              'x-ratelimit-limit': xhr.getResponseHeader('x-ratelimit-limit'),
              'x-ratelimit-remaining': xhr.getResponseHeader('x-ratelimit-remaining'),
              'x-ratelimit-reset': xhr.getResponseHeader('x-ratelimit-reset'),
            },
          });
        } else {
          reject(xhr.status);
        }
      }
    };
    xhr.send();
  });
}
