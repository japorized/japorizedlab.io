import { get } from './utils/get.js';

const errorMessage = `<i style="color: var(--color-gray)">Something went wrong and the data couldn't be fetched. The Mastodon instance might be down?</i>`;

// https://mastodon.online/api/v1/accounts/177072/statuses
async function getNewMastodonFeed(limit = 5) {
  const BASEURL = 'https://mastodon.online/api/v1';
  const USER_ID = 177072;

  const STATUSES_API_URL = `${BASEURL}/accounts/${USER_ID}/statuses${
    limit ? `?limit=${limit}` : ''
  }`;

  return get(STATUSES_API_URL, 'json');
}

function formatAttachment(attachment) {
  switch (attachment.type) {
    case 'image':
      return `<img src="${attachment.preview_url}" aria-label="${
        attachment.description ?? ''
      }" />`;
    case 'gifv':
    case 'video':
      return `<video controls loop muted src="${attachment.url}"${
        attachment.description ? ` aria-label="${attachment.description}"` : ''
      }>
      </video>`;
    default:
      return null;
  }
}

function formatPost(item) {
  // The main content that we want to show is the reblog if the "status" is a
  // reblog. Otherwise, it should just be the status itself.
  const mainContent = item.reblog ?? item;

  const mediaAttachment = (() => {
    const attachment = mainContent.media_attachments.length
      ? mainContent.media_attachments.map(formatAttachment)
      : [];

    if (attachment) {
      return `<div class="attachment-container">
        ${
          attachment.length > 1
            ? `<span class="fas fa-images multi-image-indicator"> + ${attachment.length}</span>`
            : ''
        }
        ${attachment.join('')}
      </div>`;
    }

    return null;
  })();

  const postCard = `<article class="microblog-card">
    ${
      item.reblog
        ? `<header class="microblog-card-prepend">
          <span class="fas fa-retweet" aria-label=""></span>
          ${item.account.display_name} boosted this
        </header>`
        : ''
    }
    <div class="microblog-card-body ${item.reblog ? 'reblogged' : ''}">
      <div class="microblog-card-author-image">
        <a href="${mainContent.account.url}" target="_blank">
          <img src="${mainContent.account.avatar}" />
        </a>
      </div>
      <article class="microblog-card-content">
        <h6 class="microblog-card-author">
          <a href="${mainContent.account.url}" target="_blank">
            <span>${mainContent.account.display_name}</span>
          </a>
        </h6>
        <div class="microblog-card-post">
          ${mainContent.content}

          ${mediaAttachment ? mediaAttachment : ''}
        </div>
      </article>
    </article>
  </article>`;

  return postCard;
}

export async function getFeed() {
  const POST_LIMIT = 5;
  const SEPARATOR = '===SEPARATOR===';

  // Check the cache and see if the cached content is updated
  const cachedDate = window.localStorage.getItem('feed-cache-date');
  if (cachedDate) {
    // Use the cache if its refresh within 12 hours
    if (new Date() - new Date(cachedDate) < 43200000) {
      return window.localStorage.getItem('feed').split(SEPARATOR);
    }
  }

  const rateReset = window.localStorage.getItem('feed-rate-limit-reset');
  const rateRemaining = window.localStorage.getItem('feed-rate-remaining');
  if (!Number(rateRemaining)) {
    if (new Date(rateReset) >= new Date()) {
      console.warn(
        'Rate limit exceeded requesting for feed data. Returning cache...',
      );
      return window.localStorage.getItem('feed').split(SEPARATOR);
    }
  }

  const { response: newFeed, headers } = await getNewMastodonFeed();

  const formattedFeed = newFeed
    .slice(0, POST_LIMIT) // Show only N posts
    .map(formatPost)
    .filter((item) => item !== false);

  window.localStorage.setItem('feed', formattedFeed.join(SEPARATOR));
  window.localStorage.setItem('feed-cache-date', new Date().toISOString());
  window.localStorage.setItem(
    'feed-rate-limit-reset',
    headers['x-ratelimit-reset'],
  );
  window.localStorage.setItem(
    'feed-rate-remaining',
    headers['x-ratelimit-remaining'],
  );

  return formattedFeed;
}

export async function showMastodonFeed() {
  const feed = await getFeed();
  const feedList = document.querySelector('.microblog-feed');

  feedList.innerHTML = feed.join('');
}

export function refreshMastodonFeed() {
  window.localStorage.removeItem('feed');
  window.localStorage.removeItem('feed-cache-date');
  const feedList = document.querySelector('.microblog-feed');
  feedList.innerHTML = 'Loading...';

  showMastodonFeed().catch((error) => {
    console.warn(error);
    feedList.innerHTML = errorMessage;
  });
}

showMastodonFeed().catch((_e) => {
  const feedList = document.querySelector('.microblog-feed');
  feedList.innerHTML = errorMessage;
});

document.addEventListener('DOMContentLoaded', () => {
  document
    .querySelector('#microblog-refresh')
    .addEventListener('click', refreshMastodonFeed);
});
window.addEventListener('unload', function () {
  document
    .querySelector('#microblog-refresh')
    .removeEventListener('click', refreshMastodonFeed);
});
