import path from 'path';
import { Configuration } from 'webpack';
import TerserPlugin from 'terser-webpack-plugin';

enum TargetEnv {
  Development = 'development',
  Production = 'production',
}

const env = ['dev', 'development'].includes(process.env['ENV'] ?? 'production')
  ? TargetEnv.Development
  : TargetEnv.Production;

const config: Configuration = {
  mode: env,
  entry: {
    bundle: './src/js/bundle.js',
    post: './src/js/post.js',
    blogCatalog: './src/js/blogCatalog.js',
    xBlogCatalog: './src/js/xBlogCatalog.js',
    search: './src/js/search.js',
    cv: './src/js/cv.js',
  },
  devtool: env === TargetEnv.Development ? 'inline-source-map' : false,
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin()],
  },
  output: {
    path: path.resolve(__dirname, 'assets/js'),
    sourceMapFilename: '[file].map[query]',
  },
};

export default config;
