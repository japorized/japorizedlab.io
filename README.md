# Japorized | japorized.gitlab.io 

### Special thanks to these sources
* [FontAwesome](http://fontawesome.io) for awesome icons
* Eduardo Boucas for the inspiration on Ajaxifying the Blog Index Page.
* [KaTeX](https://katex.org/) for parsing LaTeX in HTML
* [Macy.js](http://macyjs.com/) for the minimal masonry layout.
* [Lunr.js](https://lunrjs.com) for the quick static site search, right from the client's side. Thanks [RayHighTower](http://rayhightower.com/blog/2016/01/04/how-to-make-lunrjs-jekyll-work-together/) for the guide.
* [Webpack](https://webpack.js.org)
* [Stylus](https://stylus-lang.com)
* [Gulp](https://gulpjs.com)
