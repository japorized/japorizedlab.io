// Instantiates
var scroll = new SmoothScroll('a[href*="#"]', {
  speed: 500, // Integer. How fast to complete the scroll in milliseconds
  offset: 200, // Integer or Function returning an integer. How far to offset the scrolling anchor location in pixels
  easing: 'easeInOutCubic', // Easing pattern to use
  topOnEmptyHash: true
});

// Functions for after document is loaded
document.addEventListener('DOMContentLoaded', function() {
  // generic toggle function
  // usage:
  // 1. create an element that has the class toggler
  // 2. assign target by id to data-target in the same element
  // 3. make sure that the target to be toggled has the same id
  document.querySelectorAll('.toggler').forEach(function(toggle) {
    toggle.addEventListener('click', function() {
      var target = this.dataset.target,
          $target = document.getElementById(target);

      this.classList.toggle('is-active');
      $target.classList.toggle('is-active');
    });
  });

  // For the visual indicator on posts
  var $postPosition = document.querySelector('.page-position');
  if ( $postPosition != null ) {
    var $tracked = document.querySelector('.position-tracked');
    window.addEventListener('scroll', function() {
      var scrollPos = window.pageYOffset || document.body.scrollTop,
          articlelength = $tracked.clientHeight,
          percentage = scrollPos / articlelength * 100;
      $postPosition.style.width = percentage + "%";
    });
  }

  // Listener for Light-Dark Toggler
  document.querySelector('.theme-toggle').addEventListener('click', function() {
    lightdark();
  });

    // mobile navbar behaviors
  // var $navBurger = document.querySelector('.navbar-logo .navbar-item.toggler');
  // var $navbarMenuItems = Array.prototype.slice.call(document.querySelectorAll('.navbar-menu .navbar-item'), 0),
  //     navBurgerStyle = window.getComputedStyle ? getComputedStyle($navBurger, null) : $navBurger.currentStyle;
  // $navbarMenuItems.forEach(($el) => {
  //   $el.addEventListener('click', (e) => {
  //     if (navBurgerStyle.display != 'none') {
  //       $navBurger.classList.remove('is-active');
  //       document.querySelector('.navbar-menu').classList.remove('is-active');
  //     }
  //   });
  // });

});

// Collapse navbar upon scroll depending on current location relative to the entire document
window.addEventListener('scroll', function() {
  var $nav = document.querySelector('.navbar'),
      scrollPos = window.pageYOffset || document.body.scrollTop,
      navCollapseState = $nav.classList.contains('navbar-collapse');
  if (scrollPos > 50 && !navCollapseState) {
    $nav.classList.add('navbar-collapse');
  } else if (scrollPos <= 50 && navCollapseState) {
    $nav.classList.remove('navbar-collapse');
  }

});

/*
 * Functions
 */
// Light-Dark Toggler
function lightdark() {
  var curStyle = document.querySelector("#theme"),
      hljsStyle = document.querySelector("#hljs-style"),
      cvStyle = document.querySelector("#cv-style");
  if ( curStyle.href.includes('style.css') ) {
    curStyle.href = curStyle.href.replace('style.css', 'night.css');
    hljsStyle.href= hljsStyle.href.replace('gruvbox-light', 'gruvbox-dark');
    if ( cvStyle != null )
      cvStyle.href= cvStyle.href.replace('cv.css', 'cv-dark.css');
    sessionStorage.theme = "dark";
  } else {
    curStyle.href = curStyle.href.replace('night.css', 'style.css');
    hljsStyle.href= hljsStyle.href.replace('gruvbox-dark', 'gruvbox-light');
    if ( cvStyle != null )
      cvStyle.href= cvStyle.href.replace('cv-dark.css', 'cv.css');
    sessionStorage.theme = "light";
  }
}

// Search Button UI
// var $search = document.querySelector('.search'),
//     $searchBtn = document.querySelector('#searchBtn'),
//     $searchBox = document.querySelector('#search_box'),
//     $searchResults = document.querySelector('#search_results'),
//     // Function to hide search box when clicking outside of it.
//     // Unbinds the listener when the search box is hidden.
//     searchOuterClick = function(e){
//       if (e.target != $search && !Object.has($search, e.target)) {
//         if (e.target != $searchBtn) {
//           $search.classList.remove('is-active');
//         }
//       }
//     };

// Search box UX
// $searchBtn.addEventListener('click', (event) => {
//   event.preventDefault();
//   toggleSearchBox();
// });

// function toggleSearchBox() {
//   var searchBoxStyle = window.getComputedStyle ? getComputedStyle($search, null) : $search.currentStyle;
//   if ( searchBoxStyle.opacity == "0" ) {
//     $search.classList.add('is-active');
//     document.addEventListener("click", searchOuterClick);
//     $searchBox.focus();
//   } else {
//     $search.classList.remove('is-active');
//     document.removeEventListener("click", searchOuterClick);
//     $searchBox.value = "";
//     $searchResults.innerHTML = '';
//     $searchResults.style.display = 'none';
//   }
// }
