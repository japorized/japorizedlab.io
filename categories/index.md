---
layout: default
title: Categories
permalink: /categories/
---

## <span class="fa fa-folder"></span> Categories

**[personal](/categories/personal/)**  
**[siteupdate](/categories/siteupdate/)**  
**[coding](/categories/coding/)**  
**[technical](/categories/technical/)**  
**[opinion](/categories/opinion/)**  

**[software](/categories/software/)**