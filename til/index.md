---
layout: blog
pagination:
  enabled: true
  collection: til
title: TIL
---
## Today I Learned

Relatively small things that I decided to record down for reference, for myself and anyone else that comes across them.
