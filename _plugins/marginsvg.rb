## Liquid tag 'maincolumn' used to add image data that fits within the main
## column area of the layout
## Usage {% marginsvg 'margin-id-whatever' 'path/to/image' 'This is the caption' %}
#
module Jekyll
  class RenderMarginSVGTag < Liquid::Tag

  	require "shellwords"

    def initialize(tag_name, text, tokens)
      super
      @text = text.shellsplit
    end

    def render(context)
      baseurl = context.registers[:site].config['baseurl']
      assetdir = context.registers[:site].config['img_dir']
      if @text[1].start_with?('http://', 'https://', '//')
        "<label for='#{@text[0]}' class='margin-toggle'>&#8853;</label>"+
        "<input type='checkbox' id='#{@text[0]}' class='margin-toggle'/>"+
        "<span class='marginnote'><object type='image/svg+xml' data='#{@text[1]}'></object><br>#{@text[2]}</span>"
      else
        "<label for='#{@text[0]}' class='margin-toggle'>&#8853;</label>"+
        "<input type='checkbox' id='#{@text[0]}' class='margin-toggle'/>"+
        "<span class='marginnote'><object type='image/svg+xml' data='#{baseurl}/#{assetdir}/#{@text[1]}'></object><br>#{@text[2]}</span>"
      end
    end
  end
end

Liquid::Template.register_tag('marginsvg', Jekyll::RenderMarginSVGTag)
