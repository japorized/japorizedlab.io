module Jekyll
  class RenderRuby < Liquid::Tag

    require "shellwords"

    def initialize(tag_name, text, tokens)
      super
      @text = text.shellsplit
    end

    def render(context)
      "<ruby><rb>#{@text[0]}</rb><rp>(</rp><rt>#{@text[1]}</rt><rp>)</rp></ruby>"
    end
  end
end

Liquid::Template.register_tag('ruby', Jekyll::RenderRuby)
