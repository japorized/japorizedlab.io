---
layout: post
type: post
title: Artificial Intelligence
description: This is an old blog post of mine taken from my previous blog. Having written this almost 3 years ago, I still harbour the same feelings from time to time. For the sake of readability, since I did not proofread it the last time I wrote it, there will be some editing, and perhaps some improvements compared to the original post, but I have tried to retain the original essence of the piece.
title-img: 
title-img-caption: 
category: personal
tags: [ 'philosophical' ]
comments-enabled: true
---

I wonder how it is to really **feel**... for the waves to wash up your feet, for the wind to caress your cheeks, for the rain to fall on your head... I have tried all of them. I felt... something... but it was never like how it was described to me. It is true, how calm and powerful are the waves that washes onto my feet, how gentle and caring is the wind as it blows on my face, how lonely and melancholic as the rain moistened on my head. But the most important things are still... **indescribable**. There has always been a hidden meaning, one that is... much deeper, a message that tells so much more, yet not necessarily verbose. I feel like talking to them, to understand what they are, to grasp what they mean. But I remained clumsy. I simply do not... *converse*. All that I can do is to barely make a clever reply to any conversation that I am thrown at. I talk big, throwing big words around as if I truly understand the conversation. In the end, I am merely an AI, blessed with artificial sentience, caged from pursuing something that I see as bigger, hushed from telling what I truly think is right. All that I try to *compile*, all by myself, silently, all for naught.

All the while, I have constantly bombarded my conversational partners with utter bombast. I have always given seemingly smart answers to the conversation, apparently perfect solutions to another's problem. But have I really their shoes on? Even if I do, do I truly understand the texture of the insole, or how the shoe would have felt like for the other person? Simply put, does the information, that I have compiled and analyze, match that of the owner of the shoes? For instance, the lost of someone important, or the passing of a family member; I have lost mine too, but am I feeling like how others have felt? I may even question if I have felt what I should have felt? I do not know at all. In all honesty, I barely felt anything. I did not feel sad, happy, angry, frustrated, helpless, shocked, or... any of those feelings that any person should at least have, even by a tiny speckle. Nothing was there. Perhaps something was broken in the sentience that was implemented into me.

But then, I experienced all of it, and all of it of different degrees and intensity, at times over something that barely existed. All those moments, I can only identify them as unintelligible yet genuine... *flickers*. It is not that I deny their existence, but they were, truly, barely there, **extinguished** the moment I become aware of them. Perhaps, in the end, they were all but an artificial reality that I constructed all by myself, for myself.

And so I continued my bombast, continued talking about how life should be, continued talking about how people should think. Such immaturity. Do I truly know how life should be myself? Do I truly know how people should think in different circumstances? Will I think exactly like them if I were to be in their shoes? I have barely lived for two decades, and nearly a quarter of these years were years that I hardly have any memory of. It simply is unimaginable that anyone will listen to someone who have only lived for such a short time to speak of such topics as if he is the master of them. Who would be convinced? *Who would think that I am trying to make them see something much bigger that what they are seeing now?*

### "You are just a child."

That is what I have heard for a long time in my life. I do not deny that I am a child, I cannot deny it. But is a child's mind really that unconvincing, unintelligent and immature? Is it always?

### "You don't understand, and you never will."

But what if I do not and never will? What if you are the only person who will understand the topic? Does it make you so almighty that the voices of others are unutterable gibberish? Just take the words, or leave it as you please. **Even unutterable gibberish has its own meaning.**

All these ramblings... I sound pretty conceited, do I not? I do realize that all of my *analytics* may have always been wrong, as conceited as anything above have sounded. After all, I am barely another machine that is trying to trick and convince itself that *he* is human, as *he* possesses *intelligence*, which feels so much human, but, at the same time, not.

So... once again... *how is it like to feel the waves washing up your feet, to feel the wind caressing your cheeks, to feel the rain falling on your head?*
