---
layout: post
type: post
title: "Enpitsu - Dev Note #1"
description: "Note #1 on developing enpitsu. This aims to provide some coverage about developing enpitsu in JavaScript."
title-img: 
title-img-caption: 
category: coding
tags: [ 'enpitsu', 'devnotes', 'javascript' ]
comments-enabled: true
---

For quite some time now, I have been writing a command-line-based personal task
manager, mostly as a hobby project where I plough forward with my learnings about
computing, programming, and the science behind them. I have decided to start writing
about it, and make it a habit writing down my learnings, my findings, my rationale,
my reactions, etc. I think it would make for a good record of what I have done, tried,
and experienced.

First to give a brief coverage about the origins of Enpitsu. The name enpitsu is the
romanization of the Japanese word {% ruby "鉛筆" "えんぴつ" %}, which means pencil.
I figured that I wanted a simple name for the tool, which sort of acts like a trusty
pencil used to jot down things to do. I wanted the tool to be simple to use, to
understand, and to build.

<!-- more -->

At the time, I was frustrated with the direction of which JavaScript (JS), one of the
languages of my choice (still is), is heading towards; extremely gigantic, with
increasingly many gotchas and WTFs. At the same time, 
I wanted to prove to myself (okay, and a few friends) that the idea of Node.js bringing 
JS to the system is not faulty in itself. It was also because I did not really
like Python (I prefer having freedom over how I use my whitespace), and I did not get
into Ruby at the time, so having a dynamically-typed language for the system at my
disposal was big for me.

These mixed feelings led to me attempting to write a simple cli program using JS,
and for the longest time, I tried to avoid using any external libraries to help myself.
This meant that I needed to build the components that make up a cli program myself,
which included argument parsing, terminal color escape sequences,
{% sidenote 'sn-term-colors' "Oh the nightmares! Just when you thought you've figured it
out for bash, zsh and fish, shells like dash comes in with no such support! Ended up not
supporting shells that don't support these special escape sequences and just went ahead
with it."%}
input prompts in Node.js,
{% sidenote 'sn-input-prompts-node-js' "This is somehow quite messy, and I had the
impression that documentation around this was scarce or insufficient.
Many of the existing minimal solutions introduced me problems in one form or another." %}
searching in general, basic tokenization of strings, etc.
The process has also introduced and allowed me to practice on various tools that
have been developed in recent
{% sidenote 'sn-recent?' 'Well, it was rather "recent" back then.' %} years.

Fast-forward slightly to when Enpitsu in JS was near its completion.
{% sidenote 'sn-enpitsu-js' "I don't consider Enpitsu in JS as a completed project,
even today." %}
Despite being a bit of a spaghetti monster in certain parts of the code, and I certainly
did not felt that the code was well-polished, the program mostly worked as I intended it to.
It was never pushed to NPM, and I only provided a `Makefile` for installation, but through
some testing among friends, the installation process was okay.
{% sidenote 'sn-problematic-npm' "Shoutout to my friend who has nightmares installing
anything NPM. The installation was especially not smooth for him." %}
