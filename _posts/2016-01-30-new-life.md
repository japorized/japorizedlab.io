---
layout: post
type: post
title: New Life?
description: 
title-img: 
title-img-caption: 
category: personal
tags: [ 'uwaterloo' ]
comments-enabled: true
---

Time has flew by surprising fast, once again.

It's been 3 months since I last wrote here, and I just realized that today, while it still feels like this website's been made not too long ago.

#### But 3 months is a long time

And now I'm here in Canada, living my life as a student at the University of Waterloo (UW), and I've been here for a month and a week. It still feels... unreal. I'm finally here.

I'm finally back to the hectic life and being a student, juggling with studies, housework, and slowly planning for what is to come in the near future, although, I admit, that the last part is coming along at a pace lower than I would like to. It's less than 7 months till my first job now (if I can get one), and there's still so much that I need to know before that. Knowing that, though, will mean a bumpy ride awaits me.

And Winter, something that I've been foreign to for the last 20 years. I acted like a little kid when it started snowing and that everywhere was just... white, secretly stepping on piled snow or kicking the hardened ice on the pavements, then chuckling to myself. Perhaps subconsciously it reminds me of how real it is, that I am here, a place that I've been looking forward to being in, already hoping to be making new connections, and ever so anxiously.

#### But I am not the type to approach people proactively.

I'm content that just in 5 weeks, I've already made quite a number of acquaintences and, to some extent, friends. I did not hope to be able to quickly have friends that are like those in Malaysia, which I can fool around with and, at the same time, be serious over things. There hasn't been any, not yet anyways (I hope).

Life here, despite all the hecticnesss, has been quiet, which in actuality is much to my liking. The silence of the streets as I walk home in the evenings, the quiet weekends I simply indulge myself at home, alone, I like them all, perhaps a little too much, to the point that I am too content with being a student, which I know will not last forever. And then I came across this saying:


### "We should never automatically assume that the status quo is best... we should always challenge how things stand and be open to constructive change - even if it might be difficult or painful."
*- President's Message, by Stephen P. Lowe, in Actuarial Review, January - February 2016*


While the message was directed to a large audience, in particular for this case, the members of the Casualty Actuarial Society (CAS), which appears to be an insightful idea that we should all be concerned with, I am seeing it as a warning for myself, in the case of which I am content with my current lifestyle.

### The only thing that does not change, is change itself.

Or some others may just say that, "The only constant is change."

While it is possible for many to keep the change of their lifestyle to a minimum, I know that certain circumstances are impossible to not change drastically, such as the transition between study and work.

Since this change is unavoidable, and that I have no power to change it, then the change will be on me.

### How much will I change along the way?

... is still something that I wish to see for myself.
