---
layout: post
type: post
title: Making the move to Gitlab
description: Yet another migration... 😅
title-img:
title-img-caption: 
category: technical
tags: [ ]
comments-enabled: true
---

I’ve moved! Yet again! This is my second migration now; first from Blogspot to Github Pages, and now from Github Pages to Gitlab Pages.

#### <span class="fab fa-blogger"></span> $$\to$$ <span class="fab fa-github"></span> $$\to$$ <span class="fab fa-gitlab"></span>

I usually hesitate to migrate, since migration means that I would need some way of informing readers about the move. One solution is to set up a page on the old address and have them redirected to the new address, which to some extent can be an elegant solution, but it is not necessarily the best.

I did that, and you can see the page if you visit [the old address on Github](https://japorized.github.io){:target="\_blank"}.

The page is simple and easy to build, still needs a bit of work to be more mobile-friendly, but it does the job I want it to now.

<!-- more -->

---

## Why the move?

There really are only two reasons: first one being that it I can have a much cleaner repo, and the other less important one being that Github is now acquired by Microsoft.

### Lack of Confidence in Microsoft

People who know me in real life know that I am rather vocal about my dislike for Microsoft. They are doing some good things recently, e.g. trying to embrace open source, but given their track record, I find it rather difficult to trust the company in their handling of a service. Many platforms, services and/or products that they have acquired, that were successful during their time, have only seen deterioration in quality, for instance Skype and Nokia. Skype is notorious for its unreliability and resource hogging, while Nokia was basically dead, and now sold back to a company founded by a former executive of Nokia.

To be fair, it could have been Microsoft’s short-sightedness with the industry, which lead to the failure of the Windows Phone {% sidenote 'sn-id-windows-phone' "I understand that some people absolutely love their Windows Phone, but the reality is, Microsoft is slowly closing down the service (see: <a href=\"https://en.wikipedia.org/wiki/Windows_Phone\">Wikipedia</a>). It won’t be a surprise to hear an official statement about the end of the service in the next few years." %}. On the other hand, the reputation of Skype has only gone downhill ever since it was acquired by Microsoft. It is understandable that Microsoft is trying to create an ecosystem of services, but their implementations have usually been met with backlash. However, Skype is still widely used, because even today, there are no good enough alternate solutions especially for business purposes. Perhaps it is still a win for Microsoft, but my guess is that if a strong enough alternative does come out, Skype may be promptly replaced. This has already happened among the tech-savvies and gamers.

Given that, I have very little confidence in Microsoft’s ability to keep Github as a service that it is. There are definitely more reasons as to why the big migration to Gitlab from Github has occurred, but the acquisition of Microsoft is one of them.

### Pull Factors from Gitlab

Not sure if the timing is planned {% sidenote 'sn-id-gitlab-update' "See <a href=\"https://techcrunch.com/2018/06/05/gitlabs-high-end-plans-are-now-free-for-open-source-projects-and-schools\">this</a> TechCrunch article." %}, but Gitlab has rolled their own DevOps solution right in their own platform, almost right as Github was acquired by Microsoft. The introduction of these DevOps tools, especially Continuous Integration, is what has pulled me into making the switch.

Those that has poked on my repo for this website would know that I’ve struggled a lot with Travis CI, not because Travis CI is lacking, but because of the structure of which I have to maintain on Github. While Github supports Jekyll, it only supports a subset of Jekyll plugins, and one has to keep their Jekyll and Co. installation on their systems in check with what is supported by Github Pages. This also means that Github Pages is slower on adapting bug fixes, and forces its users to be confined by its limitations. And was why I looked into Travis CI to have my own solution for CI. However, given the structure of Github, since I chose to roll my own solution, I will have to maintain a branch that houses the build, and another that houses the source.

And so came the nightmare of trying to hack my way around managing two branches on the same repo that has entirely different files and even folder structure. Sure the master branch never lived in my local machine, but any time I try to build on Travis and perform other routines there (e.g. minification), something will likely go wrong.

On Gitlab, since it is integrated into the platform, all I need to do is maintain one YAML file, without the need to worry about creating a hook for triggering a remote build. Even better so, I have full control over my build tools, just as I would on Travis, the folder structure of which I get a site up and running is similar to how one would host a website. GitHub abstracts away that step, and so it becomes awkward for clean deployments.

Also, this means that I have the freedom to choose whatever static site generator I wish. I can do so with Travis, but I would still have to add a `.nojekyll` file in order to tell Github to not use Jekyll to try and build my sites. 

#### So... Fun projects ahoy!

---

Along with migrating my personal website, I have also migrated my projects over to Gitlab. The only repo that is still on Github for now is my TeX repo, but I have better plans than just making a simple migration.

I still maintain most of the imported repos on Github, which I am using Gitlab’s Mirror Repository feature to keep them in sync, especially since there are people who have starred some of my repos, particularly my [dotfiles](https://gitlab.com/japorized/dotfiles).

---

## Going Forward

This migration is simply another temporary solution, and by temporary I mean, hopefully, for the next few years. There is no telling what may happen to Gitlab in the next few years since it has gained so much traction. The best option, that may be a more permanent solution is for me to get my own domain, set up my own server, and host my website from there. But that is not what I am capable of doing right now; under present circumstances, I would rather pay for one more cup of coffee in a month, than to pay for, say, a DigitalOcean droplet.

But that day will come.
