---
layout: post
type: post
title: On Doing Things That Scale
description:
  Should we focus on doing things that scale, or is that something that is
  reserved for some purposes?
title-img:
title-img-caption:
category: opinion
tags: ['scale', 'linux', 'experience']
comments-enabled: true
---

This is an article is a reflection of my own after reading
Tobias Bernard's "Doing Things That Scale" (see his reflective blog post
[here](https://blogs.gnome.org/tbernard/2020/01/17/doing-things-that-scale/)),
and spending quite some time, usually putting it in the back burner,
thinking about his opinion.

Note that I do not claim to be in any authority in "replying" to Tobias' blog
post. And I'll express that I wholeheartedly agree on his points
on the grounds of pushing ethical technology and saner defaults to more hands.
In my blog post, there will be times where I will be playing the devil's
advocate. Please note that I am no expert on this topic, nor do I believe that
I am well-educated enough to speak for any group of people.
They are simply my own opinions that I wish to voice.

It definitely makes sense to do things that scale when your goal is to scale.
That's a no-brainer. It is clear that we shouldn't make decisions that
are not well-researched or well-studied and apply it to a wide audience, and
then expect people to be happy about it or accept it. That is simply forcing our
ideals onto others, and we know that people don't simply buy any of that,
unless if, somehow, your solution really hits the spot for a lot of people.
That is a whole other topic to leave for another day.

So perhaps your goal wasn't to go big. You didn't think that your solution
would be a coveted one; it was meant as a personal solution, but ended up
gaining attention and traction.
Well, do you want our solution to become more widely adopted?
If yes, then our goal is now to scale. Then the last paragraph applies.
If no, then let others know that they are free to fork your solution or not,
and be clear that you do not intend to continue as a leading figure in the
solution.

Some may say that it's a no brainer that we think that our solution should be
more widely adopted, because it's "great". Well then why do we think it's
great? Just because something works for us, and works really well for us,
doesn't mean that it's a great solution for "everyone".

> Just because orange juice make us feel good, happy and properly quench our
> thirst, it doesn't mean that it makes the person beside us feel good and happy,
> even if it does quench their thirst.

Okay, maybe orange juice was a bad analogy, because it has a lot of competitors:
the apple juice, watermelon juice, mango juice, and we haven't even started on
coffee, tea and pop. These products also have a lot of similarities. We can't
necessarily say that the analogy is the same as setting a different font as
default for the terminal, creating an icon that is of a certain design theme for
an application, etc.

Or was it a bad analogy after all?

There are many times when our supposed "nice solution" isn't so "nice" for an
individual person. There are even more times, when our "really good solution"
turns out to be irrelevant for a single person, and for many others.
Does it mean we should stop striving for these "really good solutions" for
ourselves, when it serves to scratch our own itch while "quenching our thirst"?

---

### Doing Things That Don't Scale

I don't consider myself a stout member of the Linux community; I am a
relatively new user to Linux (only about two years in), and barely participated
in any of the Linux circles. My impression from listening to podcasts
(Linux Unplugged is where I came across Tobias' blog post)
and lurking around some of the Linux-related subreddits is that there are many
who decided to roll with Linux because it allows for very personalized
customizations.

I argue that it is this allowance that has given Linux users the chance to
try out and explore novel ideas, and solutions, to both new and old problems.
There are many of our existing solutions that are... not good, some of which
are canonical, but we have learned to live with them.

Bash, in my opinion, is one such example. By default,

- history navigation is not friendly or discoverable; and
- completion is based on very strict matching, forcing the user to exactly
  remember the casing of directory names, filenames, and commands.

Both these items already make Bash very unappalling for many who frequent the
terminal. But because of its provenance, earlier achievements which lead to
its predominance today, and
coming as a default on many of the most prominent Linux distributions and even
on macOS,
{% sidenote 'sn-macos-bash' "I'm aware that this changed in Catalina, though
that was reasoned to be a licensing issue, not experience." %}
we have people forgoing their user experience just so they can avoid the
experience of not having their configurations when they ssh into a cloud machine,
or the same scenario when they have to move onto a new system.
{% sidenote 'sn-forging-sanity' 'I like to joke to this friend of mine by
saying, "Why do you subject yourself to this torture!?"' %}

Another example at a different level; to save a receipt on a webpage as a PDF,
you have to hit the "Print" command in your browser, and then save it as a PDF.
What does saving a file as a PDF have anything to do with printing a webpage?

I've quoted this quote before and I shall quote it again.

> The public is more familiar with bad design than good design.
> It is, in effect, conditioned to prefer bad design,
> because that is what it lives with. The new becomes threatening,
> the old reassuring.  
> ~ Paul Rand

Should everyone just decide to stick to existing solutions and defaults, there
would be little to no innovation to quality of life improvements. *It is
through the allowance of experimentation, self-expression, and plain curiosity*,
these qualities that are very close to us just **doing things that don't scale**,
that we arrive at solutions that we'd go, "Why was this not the default?" or
"I can't imagine how I used to do X before this."
Sure, there are many solutions that are clearly not scalable, but what's the
harm in that, aside from the "lost" engineering time,
{% sidenote 'sn-loss' "I will never understand why we're so obsessed with things
that we don't end up getting, and we call them losses as if belonged to us in
the first place." %}
hile giving satisfaction to the engineer?

On the other hand, trying only to do things that scale can be limiting,
time-consuming, resource-expensive, and put a damper on creativity.

Plus, not everything should scale, and you don't have to scale everything.
Just because a solution is good and works for us, doesn't mean that it has to
scale.

---

### On Defaults

All this is not to say that we should not have saner and/or nicer defaults.
We can and should push for them, while being aware of the fine line between an
opinion, a preference, and a good contemporary solution.
And defaults do not have to be dormant; it should evolve as time goes by, as we
continue to find new ways to do things, including those some of the old things
and things that we might've considered to be a done deal.

However, there's yet another balance to strike: how frequent should these
defaults be updated? Too frequent, and they would give the users a feeling of
instability and/or inconsistency; too infrequent, and we would just feel like
things aren't moving at our pace, and we go back to configuring our own setup.
To make things messier, each and every one of us has a different threshold for
"frequency".

---

To put a stop to my spiel, I'm just going to summarize the main thoughts of my
opinion piece, with some extra info that I couldn't put above.

* Do things that scale when you want or need something to scale.
* Go ahead and do things that don't scale; try out ideas, make your tool your
  tool.

If our most-used tools are supposed to be an extension of our body and mind,
then we should never need to fight them. You wouldn't fight your arms after
all. Also, you would definitely question if your arm is suitable for the person
sitting next to you, even if it's quite a "standard" arm.
