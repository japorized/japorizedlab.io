---
layout: post
type: post
title: Site Update 1
description: 
title-img: 
title-img-caption: 
category: siteupdate
tags: [ ]
comments-enabled: true
---

Our title may say Site Update #1 but the blog is already nearing its stabilizing stages.

So here's a little music for celebration, as you continue on reading this post.

<!-- more -->

{% include youtube.html youtube_id="ByEH5o4A1kc" %}

Most of what I want have already been added into the repo, and I am glad how things are turning out. Having a lot of fun as I continue to find new things to add to the blog but sooner or later, it's gonna run out? I don't think so. This blog will be maintained and slowly making tiny changes over time.

There will most likely not be large makeovers anytime soon, not if our design has become too bland and tasteless. We will go with how the rest of the Internet will be, perhaps. But I would personally stick to the minimalistic theme that I have always wanted to go with.

So here are a rundown of what has happened to the blog since the dawn of time (more specifically, the dawn of the time for this blog):

- Header background (my usual wallpaper for everywhere)
- Changed 'Career' to 'Study' (cause I'm still a student)
- Added a full About page (link in Homepage)
- Added a page for my CV (link in 'About' page)
- Refined the Navigation (when switching from 'Blog' page to 'About' page)
- Added LazyLoad Plugin for images (currently not significant)
- Personalized 404.html (retained procrastination post)
- Made links purple instead of orange (yes, me and my purple)
- Automated wrapping of embedded videos in posts (a feature that will get further updated)
- Automated tags generation & indexed tags (check out from /tags/)
- Added Secret 42 (ahem... This blog is a lab as well, and I'm hinting you where it is)

The blog started off being a normal jekyll blog actually, and I was stuck with how to design it until I met this theme. It hit me that I can do something about the theme and personalize it to my own liking.

And so I did.

*-- hereby concludes Site Update #1*
