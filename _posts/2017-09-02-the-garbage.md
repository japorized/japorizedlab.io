---
layout: post
type: post
title: The Garbage
description: “Sometimes, I'm the mess. Sometimes, I'm the broom. On the hardest days, I have to be both.” - Rudy Francisco
title-img: 
title-img-caption: 
category: personal
tags: [ ]
comments-enabled: true
---

I guess it does not help to describe an ailing mental health using phrases like “the mess” or “the garbage” metaphorically. I believe that in the next decade or so, a good portion of the world would be more aware and accepting of mental health issues as if they are physical illness, and this blog post would be condemned for its title *snicker*. (That somehow turned out to be more positive than I thought).

<div style="display: none">
---

I've been a mess for the last few months, perhaps from overworking and being sleep-deprived, probably along with the smaller factors that have entangled me for long. I wish to step away from them given my current situation and lifestyle, some of which is possible, but others are chained onto me out of the will of others. You might ask who would have that power over anyone, and you would realize the answer to that.

The world may sing praises onto such people, and expect that we sing the same praise to them too, regardless of the events that have unfolded between us and them. Yes, *regardless*. And that has empowered them to take things for granted, to do as they please and desire, allowing them to be able to convince themselves that they are rightful regardless of their actions and outcome.

#### In other words, sing your praises to them regardless of what kind of person they are and how they treat you.

They are not necessary the worst of humans, nor do they intend to be bad. They are simply people who are too tired to not being listened to, being betrayed. That has perhaps lead to who they have become. They have, perhaps, become someone that they they have never desired to be, unknowingly and unwillingly. But that cannot serve as an excuse.

#### The damage has been done and I cannot find myself in forgiving them.

I may be criticized by those who choose to believe that these people should have the full power and authority over themselves. But I could not care any longer. You can't water a long-wilted plant where its roots have shrunk and leaves have all fallen, and hope that it would one day grow back to be a strong tree, when the ground itself is unhealthy.
</div>
---

Yes I am mentally ill.

This was not intended to be a long post. Just being able to write things out helps me feel a little better and breathe a little easier. In other words, I just wanted to write something to take my mind off of things.

That is all.
