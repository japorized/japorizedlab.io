---
layout: post
type: post
title: First Post!
description: 
title-img: 
title-img-caption: 
category: personal
tags: [ ]
comments-enabled: true
---

Let's kick off the site with a geeky, developer-esque greeting...

# Hello, World!

There.

So I have finally decided to migrate from [my old blog](http://japorized.blogspot.com) from Blogspot to here on GitHub pages. Many things here at still at the stage of experiments and ideas, and I am still exploring the many (or perhaps infinite) possibilities that I can reach out to by hosting my blog from a GitHub repo.

In a more layman manner of speech, you have lurked into my evil experiment laboratory!

## HAHA

Welcome

Now to why I decided to move from Blogspot to Github Pages.

I was pretty insistent on keeping the flexibility of being able to edit the source code of the blog to my own liking, and at the same time, keep it cost-free. I would not want to host my own website on my own server, cause of security issues and my Internet stability. I would not want to find a hosting service, cause the cost would be unthinkable for a student, in Malaysia, who's only working part time and earning Malaysian Ringgit to pay in USD, monthly, and it is a sunk cost, for my own entertainment and serving very little traffic.

So I came across Github, actually months ago, even before I joined my campus hackathon and AngelHack, just cruising around the web. I only started using Github during the hackathon, and just recently, I realize that I can host my own blog on Github, with the service called Github Pages.

I was overjoyed to have found out about this. Excited, thrilled, exhilarated, think of any adjective that describes excitement.

But I had no idea how to set up one in a manageable way, cause up till recently, all I do are individual static webpages. What I mean by that is that I code up every single page **manually**. It is crazy to not have an easy way of having certain pages updating on their own, and that I do not have to constantly poke all around my repository.

So Jekyll came in, with his flapping red cape, tight blue costume, and wearing on the outside a red underwe... (don't wander off there, [I know who you're thinking](https://en.wikipedia.org/wiki/Superman))

My blogging life became so much more manageable, and actually very systematic, with Jekyll introduced as I was setting up my Github Page. It was overwhelming at first, the many configurations in the '_config.yml' file, the troubles I have had with the GemFiles... Was blindly poking everywhere and every time I check if my codes are working, I can only keep my fingers crossed that it works. Of course, many of the times, it does not work as I intended, so I just went back to tinkering.

For the longest time, I was stuck with the default Jekyll theme, and I did not dare to style it at that stage cause of how much I do not understand about it.

Now with this { Personal } theme made by Pannos Sakkos (link below in the credits), with almost all of what I need in place, plus the design, it quickly pushed my progress to more than halfway done.

And so it quickly become what you see right here before you, with almost all the basic stuff of a blog, and some other stuff that I want, in place.

Many thanks to [{ PannosSakkos }](https://github.com/PanosSakkos/personal-jekyll-theme) to have created this wonderful Jekyll theme. I will be utilizing this theme for quite some time, and will further modify it with my personal touches. Do check out the GitHub repo if you're also interested in using the theme (it's that link right up there).

I'm also utilizing a couple of libraries on the site, so a big thank you for the developers and collaborators who have made them.  
[jQuery](http://jquery.com)  
[Lazy Load Plugin](http://www.appelsiini.net/projects/lazyload)  
[FontAwesome](https://fortawesome.github.io/Font-Awesome/)  
[qTip 2](http://qtip2.com)  
[Instafeed.js](http://instafeedjs.com)

On an ending note, I will keep my old blog around, and try to maintain it if I *ever* have the time.

Hope you'd poke around some time and see you around.
