import gulp from 'gulp';
import child from 'child_process';
import gulpUglify from 'gulp-uglify';
import stylus from 'gulp-stylus';
import log from 'fancy-log';
import postcss from 'gulp-postcss';
import autoprefixer from 'autoprefixer';
import cssnano from 'cssnano';
import webpack from 'webpack';
import webpackConfigs from './webpack.config';

const ASSET_JS = 'assets/js/';
const BROWSER = '/usr/bin/firefox';

// var messages = {
//   jekyllBuild: '<span style="color: grey">Running:</span> $ jekyll build',
// };

// Commonly repeated jobs
gulp.task('generateTags', (done) => {
  child.spawn('scripts/generate-tags', [''], { stdio: 'inherit' });
  child.spawn('scripts/generate-categories', [''], { stdio: 'inherit' });
  done();
});

// Regular Update Tasks
gulp.task('installJSLibs', (done) => {
  gulp
    .src(['node_modules/typelighterjs/typelighter.min.js'])
    .pipe(gulpUglify())
    .pipe(gulp.dest(ASSET_JS));

  done();
});

// Build Tasks
gulp.task('stylus', function (done) {
  const postcss_plugins = [autoprefixer(), cssnano()];
  gulp
    .src([
      'src/css/light.styl',
      'src/css/dark.styl',
      'src/css/colors.styl',
      'src/css/base.styl',
      'src/css/blogCatalog.styl',
      'src/css/cv.styl',
      'src/css/search.styl',
      'src/css/categories.styl',
      'src/css/tags.styl',
      'src/css/projects.styl',
    ])
    .pipe(
      stylus({
        compress: true,
      }),
    )
    .on('error', function (err) {
      log(err.toString());
    })
    .pipe(postcss(postcss_plugins))
    .pipe(gulp.dest('assets/css'));
  done();
});

function logthis(cprocess: child.ChildProcess, name: string) {
  const logger = (buffer: Buffer) => {
    buffer
      .toString()
      .split(/\n/)
      .forEach((message) => log(`${name}: ${message}`));
  };

  cprocess.stdout?.on('data', logger);
  cprocess.stderr?.on('data', logger);
}

gulp.task('jekyll', (done) => {
  const jekyll = child.spawn('bundle', ['exec', 'jekyll', 'serve', '--watch']);
  logthis(jekyll, 'Jekyll');
  done();
});

gulp.task('jekyll-build', (done) => {
  child.spawn('bundle', ['exec', 'jekyll', 'build']);
  done();
});

gulp.task('javascript', (done) => {
  // const webpack = child.spawn('npm', ['run', 'webpack:dev:nowatch']);
  // logthis(webpack, 'Webpack');
  const runner = webpack(webpackConfigs);
  runner.run(() => {});
  done();
});

gulp.task('start-browser', (done) => {
  child.spawn(BROWSER, ['127.0.0.1:4000'], { stdio: 'inherit' });
  done();
});

// Pipelined jobs
gulp.task('watch', (done) => {
  gulp.watch(['src/css/**/*.styl'], gulp.series('stylus'));
  gulp.watch(
    ['webpack.config.ts', 'src/js/**/*.js'],
    gulp.series('javascript'),
  );
  gulp.watch('assets/**/*', gulp.series('jekyll-build'));
  done();
});

gulp.task(
  'build',
  gulp.series('javascript', 'jekyll-build'),
);

gulp.task(
  'deploy',
  gulp.series('generateTags', 'stylus', 'javascript', (done) => {
    log('[assistant] Ready to be shipped!');
    done();
  }),
);
gulp.task(
  'default',
  gulp.series('installJSLibs', 'stylus', 'javascript', 'jekyll', 'watch'),
);
