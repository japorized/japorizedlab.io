export default function lightdark() {
  const curStyle = document.querySelector('#theme');
  const hljsStyle = document.querySelector('#hljs-style');

  if (curStyle === null) {
    const base = document.querySelector('#base-theme');
    const colorLink = document.createElement('link');
    const hljsLink = document.createElement('link');
    colorLink.rel = 'stylesheet';
    colorLink.id = 'theme';
    hljsLink.rel = 'stylesheet';
    hljsLink.id = 'hljs-style';

    if (window.matchMedia('(prefers-color-scheme: light)').matches) {
      colorLink.href = '/assets/css/dark.css';
      hljsLink.href =
        '//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.14.2/styles/gruvbox-dark.min.css';
      sessionStorage.theme = 'dark';
    } else {
      colorLink.href = '/assets/css/light.css';
      hljsLink.href =
        '//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.14.2/styles/gruvbox-light.min.css';
      sessionStorage.theme = 'light';
    }

    base.parentNode.insertBefore(colorLink, base.nextSibling);
    base.parentNode.insertBefore(hljsLink, base.nextSibling);
  } else {
    if (curStyle.href.includes('light.css')) {
      // light -> dark
      curStyle.href = curStyle.href.replace('light.css', 'dark.css');
      hljsStyle.href = hljsStyle.href.replace('gruvbox-light', 'gruvbox-dark');
      sessionStorage.theme = 'dark';
    } else {
      // dark -> light
      curStyle.href = curStyle.href.replace('dark.css', 'light.css');
      hljsStyle.href = hljsStyle.href.replace('gruvbox-dark', 'gruvbox-light');
      sessionStorage.theme = 'light';
    }
  }
}
