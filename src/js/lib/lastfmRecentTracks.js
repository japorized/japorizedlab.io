import get from '../utils/get';

export default function getLastFMRecentTracks() {
  const lastfmUser = 'johnson5756';
  const lastfmReqLimit = 15;
  const apikey = '05639839701ccb3b35cfaf6bd2944037';
  const base_url = `https://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&user=${lastfmUser}&limit=${lastfmReqLimit}&api_key=${apikey}&format=json`;

  get(base_url, 'json').then(function (data) {
    const showLimit = 3;
    const albumCoverSize = 'large';
    const recentTracks = [];
    const includedAlbums = [];

    // Pick the tracks in a way such that all 3 tracks are not from the same album
    // However, we since we do not want to make big requests,
    // we'll always pick the last remaining items to show
    // even if they are of the same album
    for (let i = 0; i < data.recenttracks.track.length; i++) {
      if (recentTracks.length < showLimit) {
        if (lastfmReqLimit - i >= showLimit) {
          const iteratedAlbum = data.recenttracks.track[i].album['#text'];
          if (includedAlbums.includes(iteratedAlbum)) {
            continue;
          }
        }

        recentTracks.push(data.recenttracks.track[i]);
      } else {
        break;
      }
    }

    if (recentTracks.length > 0) {
      document.querySelector('.lastfm-displayText').classList.add('active');
      const recenttrackContainer = document.querySelector('.lastfm-recent');
      recentTracks.forEach((track) => {
        const title = track.name;
        const artist = track.artist['#text'];
        const lastFmUrl = track.url;
        let [albumArtUrl] = [
          ...track.image.filter((image) => image.size === albumCoverSize),
        ];
        if (albumArtUrl['#text'] === undefined) {
          albumArtUrl['#text'] = '/assets/img/noartwork.jpg';
        }

        let song = `<div class="track g-col-4-s">
          <a href="${lastFmUrl}" target="_blank">
            <div class="album-cover">
              <img src="${albumArtUrl['#text']}" />
            </div>
            <div class="trackDesc">
              <div class="track-artist">${artist}</div>
              <div class="track-name">${title}</div>
            </div>
          </a>
          </div>`;

        recenttrackContainer.innerHTML += song;
        recenttrackContainer.classList.add('active');
      });
    }
  });
}
