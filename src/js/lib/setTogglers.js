function toggle() {
  const target = document.getElementById(this.dataset.target);

  this.classList.toggle('is-active');
  target.classList.toggle('is-active');
}

export function setTogglers() {
  document.querySelectorAll('.toggler').forEach(function (t) {
    t.addEventListener('click', toggle);
  });
}

export function unsetTogglers() {
  document.querySelectorAll('.toggler').forEach(function (t) {
    t.removeEventListener('click', toggle);
  });
}
