function ReplyToComment(id, author) {
  // update hidden form entries for a reply
  const form = document.querySelector('#post-new-comment');
  const formParent = form.parentNode;
  const replyIdInput = form.querySelector('#replyIdInput');
  const replyNameInput = form.querySelector('#replyNameInput');
  replyIdInput.value = id;
  replyNameInput.value = author;

  // highlight comment that is being replied to
  // first, remove other being-replied-to comment, if any
  // and if there is any, a cancel button and go back button
  // should be there, and so we shall remove those as well
  const beingRepliedTo = document.querySelector('.being-replied-to');
  if (beingRepliedTo != null) {
    beingRepliedTo.classList.remove('being-replied-to');
    formParent.removeChild(formParent.querySelector('.reply-cancel'));
    formParent.removeChild(formParent.querySelector('.back-to-comment'));
  }

  const parentComment = document.querySelector('#comment-' + id);
  parentComment.classList.add('being-replied-to');

  // change heading for new comment to show
  // that we have switched to reply mode
  const heading = document.querySelector('#post-new-comment-heading');
  heading.innerHTML = 'Replying to ' + author + ' ';

  // create a button to go back to reply
  const goBack = document.createElement('button');
  goBack.innerHTML = 'Go back to comment';
  goBack.classList.add('back-to-comment');
  goBack.addEventListener('click', function () {
    parentComment.scrollIntoView({ behavior: 'smooth' });
  });

  // create a cancel button
  // cancel button should restore the form to
  // new comment mode
  // 1. reset replyId and replyName to ""
  // 2. restore "Replying to xxx" to "Post a new comment"
  // 3. remove highlight on comment that was replied to
  // 4. remove the go back button and itself
  const cancelBtn = document.createElement('button');
  cancelBtn.innerHTML = 'Cancel reply';
  cancelBtn.classList.add('reply-cancel');
  cancelBtn.addEventListener('click', function () {
    this.parentNode.removeChild(goBack);
    this.parentNode.removeChild(this);
    parentComment.classList.remove('being-replied-to');
    heading.innerHTML = 'Post a new comment';
    replyIdInput.value = '';
    replyNameInput.value = '';
  });

  // show cancel button and go back to comment button
  // before the form itself
  formParent.insertBefore(cancelBtn, form);
  formParent.insertBefore(goBack, form);

  // finally, scroll to where the comment box is
  heading.scrollIntoView({ behavior: 'smooth' });
}

function CopyURLtoComment(url) {
  navigator.clipboard.writeText(url).then(
    function () {
      let tinyalert = document.createElement('button');
      tinyalert.innerHTML = 'Copied comment URL to clipboard';
      tinyalert.classList.add('alertbox', 'pre-entry');
      tinyalert.addEventListener('click', function () {
        this.parentNode.removeChild(this);
      });
      document.body.appendChild(tinyalert);
      setTimeout(function () {
        tinyalert.classList.remove('pre-entry');
      }, 5);
      setTimeout(function () {
        tinyalert.parentNode.removeChild(tinyalert);
      }, 5000);
    },
    function (err) {
      console.error('Async: Could not copy text: ', err);
    },
  );
}
