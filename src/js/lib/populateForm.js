import get from '../utils/get';

export default function populateFormValidationData() {
  get('/assets/form-check.json', 'json').then(function (json) {
    const question = document.querySelector('#form-question-label');
    if (!question) return;
    const selection = document.querySelector('#form-question');
    if (!selection) return;
    const r = Math.floor(Math.random() * objsize(json));
    question.innerHTML += json[r].q;

    for (const c of json[r].s) {
      let el = `<option value="${c}">${c}</option>`;
      selection.innerHTML += el;
    }

    // save the json on session for validation
    sessionStorage.form = JSON.stringify(json);
  });
}

function objsize(obj) {
  return Object.keys(obj).length;
}
