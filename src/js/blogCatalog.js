import Macy from 'macy';
import LazyLoad from 'vanilla-lazyload';
import get from './utils/get';

document.addEventListener('DOMContentLoaded', () => {
  // start macy
  const msnry = new Macy({
    container: '.masonry',
    columns: 2,
    margin: {
      x: 50,
      y: 100,
    },
    breakAt: {
      780: {
        margin: {
          x: 0,
          y: 0,
        },
        columns: 1,
      },
    },
  });

  const lzload = new LazyLoad({
    callback_loaded: function () {
      msnry.recalculate();
    },
  });

  // AJAX load more post
  // Inspired by Eduardo Boucas (@eduardoboucas)
  const loader = document.querySelector('.loadMore');
  loader.innerHTML = 'Load more posts';
  loader.removeAttribute('href');
  loader.addEventListener('click', function (event) {
    event.preventDefault();
    const $this = this;
    const $blogContainer = document.getElementById('blog_container');
    const nextPage = parseInt($blogContainer.getAttribute('data-curPage')) + 1;
    const totalPages = parseInt($blogContainer.getAttribute('data-totalPages'));

    $this.classList.add('loading');
    $this.innerHTML = '. . .';

    const pathArr = location.href.split('/');
    const directory = pathArr[3];

    get('/' + directory + '/page' + nextPage + '/').then(
      function (data) {
        window.setTimeout(function () {
          const doc = parseHTML(data);
          const posts = doc
            .namedItem('blog-outer-container')
            .querySelectorAll('article.masonry-item');
          const articleFrags = document.createDocumentFragment();

          $blogContainer.setAttribute('data-curPage', nextPage);

          posts.forEach(function (article) {
            article.style = '';
            article.classList.add('msnry-animate');
            articleFrags.appendChild(article);
          });

          const postMsnry = document.querySelector('.masonry');
          postMsnry.appendChild(articleFrags);
          lzload.update();
          msnry.recalculate();
          postMsnry
            .querySelectorAll('.msnry-animate')
            .forEach(function (article) {
              article.classList.remove('msnry-animate');
            });

          if (totalPages == nextPage) {
            loader.parentNode.removeChild(loader);
          } else {
            $this.innerHTML = 'Load more posts';
            $this.classList.remove('loading');
          }
        }, 300);
      },
      function (xhr) {
        console.log(xhr);
      },
    );
  });
});

function gotoPost(url) {
  window.location.href = url;
}

function parseHTML(str) {
  const tmp = document.implementation.createHTMLDocument();
  tmp.body.innerHTML = str;
  return tmp.body.children;
}
