const titles = document.querySelector('#cv').querySelectorAll('[id]');
const anchorsList = document.querySelector('.anchors-list');
titles.forEach((title) => {
  const div = document.createElement('div');
  div.innerHTML = `<a href="#${title.id}">${title.innerHTML}</a>`;
  anchorsList.append(div);
});
