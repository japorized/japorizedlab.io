import * as SmoothScroll from 'smooth-scroll';
import hljs from 'highlight.js';

import { setTogglers, unsetTogglers } from './lib/setTogglers';
import lightdark from './lib/lightdark';
import getLastFMRecentTracks from './lib/lastfmRecentTracks';

function onloads() {
  setTogglers();
  getLastFMRecentTracks();
  hljs.initHighlightingOnLoad();
  document.querySelector('.theme-toggle').addEventListener('click', lightdark);
}

function unloads() {
  unsetTogglers();

  document
    .querySelector('.theme-toggle')
    .removeEventListener('click', lightdark);

  document.removeEventListener('DOMContentLoaded', onloads);
}

new SmoothScroll('a[href^="#"]', {
  speed: 500, // Integer. How fast to complete the scroll in milliseconds
  offset: 200, // Integer or Function returning an integer.
  // How far to offset the scrolling anchor location in pixels
  easing: 'easeInOutCubic', // Easing pattern to use
  topOnEmptyHash: true,
});

document.addEventListener('DOMContentLoaded', onloads);
window.addEventListener('unload', unloads);

window.addEventListener('scroll', function () {
  const $nav = document.querySelector('.navbar');
  const scrollTop = window.pageYOffset || document.body.scrollTop;
  const navCollapseState = $nav.classList.contains('navbar-collapse');
  if (scrollTop > 50 && !navCollapseState) {
    $nav.classList.add('navbar-collapse');
  } else if (scrollTop <= 50 && navCollapseState) {
    $nav.classList.remove('navbar-collapse');
  }
});
