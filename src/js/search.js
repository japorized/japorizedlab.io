import lunr from 'lunr';
import get from './utils/get';

document.addEventListener('DOMContentLoaded', function () {
  document
    .querySelector('#advanced-help')
    .addEventListener('click', function (f) {
      f.preventDefault();
      var searchHelp = document.querySelector('.search-help');
      if (
        searchHelp.style.display == 'none' ||
        searchHelp.style.display === ''
      ) {
        searchHelp.style.display = 'block';
      } else {
        searchHelp.style.display = 'none';
      }
    });
});

window.data = get('/searchData.json', 'json');

window.data.then(function (loaded_data) {
  var searchform = document.getElementById('site_search');
  searchform.addEventListener('submit', function (e) {
    e.preventDefault();
  });
  searchform.addEventListener('keyup', function () {
    var query = document.getElementById('search_box').value;
    if (query !== '') {
      var results = idx.search(query);
      display_search_results(results);
      document.getElementById('search_results').style.display = 'block';
    } else {
      document.getElementById('search_results').style.display = 'none';
    }
  });

  window.idx = lunr(function () {
    this.field('id');
    this.field('title');
    this.field('description');
    this.field('content', { boost: 10 });
    this.field('categories');
    this.field('tags');

    var i = 0;
    for (var value in loaded_data) {
      if (loaded_data.hasOwnProperty(value)) {
        var content = JSON.parse(
          JSON.stringify(Object.assign({}, loaded_data[value], { id: i++ })),
        );
        this.add(content);
      }
    }
  });
});

function display_search_results(results) {
  var $search_results = document.getElementById('search_results');

  // Wait for data to load
  window.data.then(function (loaded_data) {
    // Are there any results?
    if (results.length) {
      $search_results.innerHTML = ''; // Clear old results

      // Iterate over the results
      results.forEach(function (result) {
        var item = loaded_data[Object.keys(loaded_data)[result.ref]];

        // Build a snippet of HTML for this result
        var appendString =
          '<li><a href="' + item.url + '">' + item.title + '</a></li>';

        // Add the snippet to the collection of results.
        $search_results.innerHTML += appendString;
      });
    } else {
      // If there are no results, let the user know.
      $search_results.innerHTML = '<li>No results found</li>';
    }
  });
}
