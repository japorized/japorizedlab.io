import LazyLoad from 'vanilla-lazyload';

document.addEventListener('DOMContentLoaded', () => {
  new LazyLoad();
});

window.addEventListener('unload', () => {
  document.removeEventListener('DOMContentLoaded');
});
