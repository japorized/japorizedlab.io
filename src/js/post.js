import populateFormValidationData from './lib/populateForm';
import './lib/commenting';

function newCommentValidation() {
  // HoneyPots
  const hp = document.querySelector('#comment-website');
  if (hp.value.length > 0) {
    return false;
  }

  const url_field = document.querySelector('#form-url');
  if (url_field.value.length > 0) {
    return false;
  }

  // Custom captcha
  const json = JSON.parse(sessionStorage.form);

  const formQuestionLabel = document.querySelector(
    '#form-question-label',
  ).innerHTML;
  if (!formQuestionLabel) return false;

  const selection = document.querySelector('#form-question');
  if (!selection) return false;

  for (const key in json) {
    if (json[key].q === formQuestionLabel) {
      if (json[key].a === selection.value) {
        return true;
      }

      return false;
    }
  }

  return false;
}

const postPosition = document.querySelector('.page-position');
if (postPosition != null) {
  const tracked = document.querySelector('.position-tracked');
  window.addEventListener('scroll', function () {
    const scrollPos = window.pageYOffset || document.body.scrollTop;
    const articlelength = tracked.clientHeight;
    const percentage = (scrollPos / articlelength) * 100;
    postPosition.style.width = percentage + '%';
  });
}

document.addEventListener('DOMContentLoaded', populateFormValidationData);
