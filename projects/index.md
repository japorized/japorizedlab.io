---
layout: projects
type: catalog
title: projects
projects: 
  - title: TeX Notes
    url: https://tex.japorized.ink
    img: tex_notes.jpg
    description: My workflow for LaTeX. Repo is online, with a front page (linked).
  - title: enpitsu
    url: https://gitlab.com/enpitsu/enpitsu-js
    img: enpitsu.jpg
    description: An experimental terminal personal task manager written in Javascript.
  - title: elementary
    url: https://japorized.gitlab.io/elementary
    img: elementary.jpg
    description: A simple personal startpage using simple HTML, CSS and Javascript.
  - title: vim-template
    url: https://gitlab.com/japorized/vim-template
    img: vim-template.jpg
    description: A vim plugin that provides a very basic templating capability to vim. Optionally dependent on Denite.nvim.
  - title: rustdown
    url: https://gitlab.com/japorized/rustdown
    img:
    description: My experimental non-standards compliant markdown to HTML converter, written using Rust.
  - title: rEnpitsu
    url: https://gitlab.com/enpitsu/enpitsu-cli
    img:
    description: (WIP) Enpitsu rewritten in Rust.
---

## Projects and Showcases
